#!/bin/bash

# get dir
DIR=$(cd $(dirname $0) && pwd)
SRC_DIR=$DIR/src
DEST_DIR=$DIR/dest

# make file in src folder
cd $SRC_DIR 
make clean
make all

# link binaries to dest folder
rm -rf $DEST_DIR
mkdir $DEST_DIR
cd $DEST_DIR;
ln -s $SRC_DIR/mainParent . 
ln -s $SRC_DIR/childA .
ln -s $SRC_DIR/childB .

# clean existed prcoesses and run mainParent
ps -ef | grep -E 'mainParent|childA|childB' | awk '{print $2}' | xargs kill -9
cd $DEST_DIR; ./mainParent
