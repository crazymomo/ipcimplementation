//
//  mainParent.cpp
//  Parent
//
//  Created by momo on 2017/8/20.
//  Copyright © 2017年 momo. All rights reserved.
//

#include <stdio.h>

#include <assert.h>

#include "CalculationService.hpp"
#include "Logger.hpp"

#include "Algorithm.hpp"

int main(int argc, char** argv) {
    // init logger
    hlx::Logger::getInstance() -> init(std::string(argv[0]));
    
    // show logs
    hlx::Logger::getInstance() -> normal(true, "Main Process Started\n");
    
    // init calculation service
    hlx::CalculationService::getInstance() -> init();
    
    // read from stdin
    while (true) {
        // wait numberv
        hlx::Logger::getInstance() -> normal(true, "Enter a positive integer or 0 to exit: ");
        int number;
        std::cin >> number;
        
        // do exit
        if (number == 0) {
            hlx::CalculationService::getInstance() -> exit();
            break ;
        }
        // do calculation
        else {
            hlx::CalculationService::getInstance() -> doCalculation(number);
        }
    }
    
    // show logs
    hlx::Logger::getInstance() -> normal(true, "Process Exits\n");
    
    return 0;
}
