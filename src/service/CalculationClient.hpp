//
//  CalculationClient.hpp
//  ChildWithPipe
//
//  Created by momo on 2017/8/30.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef CalculationClient_hpp
#define CalculationClient_hpp

#include <stdio.h>

#include "Observer.hpp"
#include "CommandHandler.hpp"

namespace hlx {
    class CalculationClient : public Observer {
    public:
        CalculationClient(std::string type);
        
        // update
        virtual void update(std::shared_ptr<Command> command);
        
        // start
        virtual void start();
        
    protected:
        // handler
        std::shared_ptr<CommandHandler> _handler;
        
        // value list
        std::vector<int> _valueList;
    };
};

#endif /* CalculationClient_hpp */
