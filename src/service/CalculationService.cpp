//
//  CalculationService.cpp
//  Parent
//
//  Created by momo on 2017/8/20.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "CalculationService.hpp"

#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

#include <assert.h>

#include "Config.hpp"
#include "Logger.hpp"

hlx::CalculationService* hlx::CalculationService::_singleton = NULL;

hlx::CalculationService* hlx::CalculationService::getInstance() {
    if (_singleton == NULL) {
        static std::mutex mutex;
        std::unique_lock<std::mutex> lock(mutex);
        if (_singleton == NULL) {
            _singleton = new hlx::CalculationService();
        }
    }
    return _singleton;
}

hlx::CalculationService::CalculationService() {
    // init random seed
    srand((unsigned int) time(NULL));
}

void hlx::CalculationService::update(std::shared_ptr<Command> command) {
    // add lock to make sure this is atomic control
    std::unique_lock<std::mutex> lock(_mutex);
    switch (command -> _type) {
        case Command::END_INIT:
            _initCV.notify_all();
            break;
        case Command::END_CALC:
            _calcCV.notify_all();
            break;
        default:
            assert(false);
            break;
    }
}

void hlx::CalculationService::addCommandList(std::unordered_map<std::string, std::shared_ptr<CommandHandler>> handlerMap, std::unordered_map<std::string, std::vector<Command::TYPE> > handlerCommandMap) {
    // add commands
    for (auto itr = handlerMap.begin(); itr != handlerMap.end(); itr ++) {
        for (auto cmd_itr = handlerCommandMap[itr -> first].begin(); cmd_itr != handlerCommandMap[itr -> first].end(); cmd_itr ++) {
            itr -> second -> doWrite(std::shared_ptr<Command>(new Command(*cmd_itr)));
        }
        
    }
}

// ======================== commands ========================

void hlx::CalculationService::init() {
    // add lock to make sure this is atomic control
    std::unique_lock<std::mutex> lock(_mutex);
    
    // init child list
    _childList.clear();
    _childProcessMap.clear();
    _handlerMap.clear();
    _handlerPrefixCommandMap.clear();
    _handlerPostfixCommandMap.clear();
    std::unordered_map<std::string, std::vector<Command::TYPE>> prefixCommandListMap;
    std::unordered_map<std::string, std::vector<Command::TYPE>> postfixCommandListMap;
    
    // set child a
    {
        std::string child = "childA";
        _childList.push_back(std::pair<std::string, std::string>(child, Config::PIPE_TYPE));
        prefixCommandListMap[child] = std::vector<Command::TYPE>{Command::CLEAR, Command::WILL_ADD};
        postfixCommandListMap[child] = std::vector<Command::TYPE>{Command::DID_ADD, Command::CALC_MEDIAN};
    }
    
    // set child b
    {
        std::string child = "childB";
        _childList.push_back(std::pair<std::string, std::string>(child, Config::SHM_TYPE));
        prefixCommandListMap[child] = std::vector<Command::TYPE>{Command::CLEAR, Command::WILL_ADD};
        postfixCommandListMap[child] = std::vector<Command::TYPE>{Command::DID_ADD, Command::SORT, Command::CALC_GEOMETRIC_MEAN};
    }

    // create child process and relative command handler
    for (auto itr = _childList.begin(); itr != _childList.end(); itr ++) {
        // get child process
        int pid = fork();
        assert(pid != -1);
        _childProcessMap[itr -> first] = pid;
        
        // child
        if (pid == 0) {
            // show logs
            Logger::getInstance() -> normal(true, "%s Process Created\n", itr -> first.c_str());
            
            // launch child
            char cmd[1024];
            sprintf(cmd, "./%s", itr -> first.c_str());
            system(cmd);
            
            // exit
            ::exit(0);
        }
        // parent
        else {
            // launch command handler and wait init
            _handlerMap[itr -> first] = std::shared_ptr<CommandHandler>(new CommandHandler(itr -> second));
            _handlerMap[itr -> first] -> addObserver(this);
            _initCV.wait(lock);
            
            // prefix command list and postfix list
            _handlerPrefixCommandMap[itr -> first] = prefixCommandListMap[itr -> first];
            _handlerPostfixCommandMap[itr -> first] = postfixCommandListMap[itr -> first];
        }
    }
    
    // show logs
    Logger::getInstance() -> normal(false, "\n");
}

void hlx::CalculationService::exit() {
    // add lock to make sure this is atomic control
    std::unique_lock<std::mutex> lock(_mutex);
    
    // show log
    Logger::getInstance() -> normal(true, "Process Waits\n");
    
    // add commands
    for (auto itr = _handlerMap.begin(); itr != _handlerMap.end(); itr ++) {
        itr -> second -> doWrite(std::shared_ptr<Command>(new Command(Command::EXIT)));
    }
    
    // wait all childs are gone
    while (_childProcessMap.size() > 0) {
        for (auto itr = _childProcessMap.begin(); itr != _childProcessMap.end(); itr ++) {
            // get exit code
            int exit;
            waitpid(itr -> second, &exit, 0);
            
            // check exit
            if (exit == 0) {
                _childProcessMap.erase(itr);
                break ;
            }
        }
    }
}

void hlx::CalculationService::doCalculation(int random_number) {
    // add lock to make sure this is atomic control
    std::unique_lock<std::mutex> lock(_mutex);
    
    // add prefix command
    addCommandList(_handlerMap, _handlerPrefixCommandMap);

    // show log
    Logger::getInstance() -> normal(true, "Generating %d random integers: ", random_number);
    
    // add random interger between 50 ~ 100
    for (auto i = 0; i < random_number; i ++) {
        // get value
        int value = rand() % 51 + 50;
        
        // show log
        Logger::getInstance() -> normal(false, "%d ", value);
        
        // add commands
        for (auto itr = _handlerMap.begin(); itr != _handlerMap.end(); itr ++) {
            itr -> second -> doWrite(std::shared_ptr<Command>(new Command(Command::ADD, value)));
        }
    }
    
    // show log
    Logger::getInstance() -> normal(false, "\n\n");
    
    // add posftix command
    addCommandList(_handlerMap, _handlerPostfixCommandMap);
    
    // wait two clients
    _calcCV.wait(lock);
    _calcCV.wait(lock);
}
