//
//  CalculationService.hpp
//  Parent
//
//  Created by momo on 2017/8/20.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef CalculationService_hpp
#define CalculationService_hpp

#include <stdio.h>

#include <semaphore.h>
#include <mutex>
#include <condition_variable>

#include <vector>
#include <unordered_map>

#include "Observer.hpp"
#include "CommandHandler.hpp"

namespace hlx {
    class CalculationService : public Observer {
    public:
        // singleton
        static CalculationService* getInstance();
        
        virtual void update(std::shared_ptr<Command> command);
        
        // do init
        void init();
        // do exit
        void exit();
        // do a calculation
        void doCalculation(int random_number);
        
    protected:
        CalculationService();
        static CalculationService* _singleton;
        void addCommandList(std::unordered_map<std::string, std::shared_ptr<CommandHandler>> handlerMap,  std::unordered_map<std::string, std::vector<Command::TYPE>> handlerCommandMap);
        
        // process and handler list
        std::vector<std::pair<std::string, std::string>> _childList;
        std::unordered_map<std::string, int> _childProcessMap;
        std::unordered_map<std::string, std::shared_ptr<CommandHandler>> _handlerMap;
        std::unordered_map<std::string, std::vector<Command::TYPE>> _handlerPrefixCommandMap;
        std::unordered_map<std::string, std::vector<Command::TYPE>> _handlerPostfixCommandMap;
        
        // mutex
        std::mutex _mutex;
        std::condition_variable _initCV;
        std::condition_variable _calcCV;
    };
}

#endif /* CalculationService_hpp */
