//
//  CalculationClient.cpp
//  ChildWithPipe
//
//  Created by momo on 2017/8/30.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "CalculationClient.hpp"

#include <algorithm>

#include <assert.h>

#include "PipeBroker.hpp"
#include "SharedMemoryBroker.hpp"

#include "Config.hpp"
#include "Algorithm.hpp"
#include "Logger.hpp"

hlx::CalculationClient::CalculationClient(std::string type) {
    // set handler
    assert(type == Config::PIPE_TYPE || type == Config::SHM_TYPE);
    _handler = std::shared_ptr<CommandHandler>(new CommandHandler(type));
    
    // set observer
    _handler -> addObserver(this);
}

void hlx::CalculationClient::start() {
    // send end_init cmd
    _handler -> doWrite(std::shared_ptr<hlx::Command>(new hlx::Command(hlx::Command::END_INIT)));
    
    // start read
    _handler -> doRead();
}

void hlx::CalculationClient::update(std::shared_ptr<Command> command) {
    // parse command
    switch (command -> _type) {
        case Command::NONE:
            // doesn't expect to receive this command
            assert(false);
            break;

        case Command::WILL_ADD:
            // show logs
            Logger::getInstance() -> normal(true, "Random Numbers Received From %s: ", _handler -> getDisplayStr().c_str());
            break;

        case Command::DID_ADD:
            // show logs
            Logger::getInstance() -> normal(false, "\n", _handler -> getDisplayStr().c_str());
            break ;

        case Command::ADD:
            // show logs
            Logger::getInstance() -> normal(false, "%d ", command -> _value);
            _valueList.push_back(command -> _value);
            break;

        case Command::SORT:
            std::sort(_valueList.begin(), _valueList.end());
            // show logs
            Logger::getInstance() -> normal(true, "Sorted Sequence: ");
            for (auto itr = _valueList.begin(); itr != _valueList.end(); itr ++) {
                // show logs
                Logger::getInstance() -> normal(false, "%d ", *itr);
            }
            // show logs
            Logger::getInstance() -> normal(false, "\n");
            break;

        case Command::CALC_MEDIAN:
            // show logs
            Logger::getInstance() -> normal(true, "Median: %f\n\n", Algorithm::calcMedian(_valueList));
            // send end_calc cmd
            _handler -> doWrite(std::shared_ptr<hlx::Command>(new hlx::Command(hlx::Command::END_CALC)));
            break;

        case Command::CALC_GEOMETRIC_MEAN:
            // show logs
            Logger::getInstance() -> normal(true, "Geometric Mean: %f\n\n", Algorithm::calcGeometricMean(_valueList));
            // send end_calc cmd
            _handler -> doWrite(std::shared_ptr<hlx::Command>(new hlx::Command(hlx::Command::END_CALC)));
            break;

        case Command::CLEAR:
            _valueList.clear();
            break;

        case Command::EXIT:
            _handler -> doClose();
            break;
            
        default:
            // miss command
            assert(false);
            break;
    }
}
