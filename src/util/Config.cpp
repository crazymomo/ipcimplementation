//
//  Config.cpp
//  Parent
//
//  Created by momo on 2017/8/28.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "Config.hpp"

// ==================== generic ====================

const std::string hlx::Config::FOLDER               = ".";

const std::string hlx::Config::SEPERATOR            = "/";

const std::string hlx::Config::UNDERLINE            = "_";

const std::string hlx::Config::DOT                  = ".";

const std::string hlx::Config::LOCK                 = "lock";


// ==================== shared memory ====================

const std::string hlx::Config::SHM_TYPE             = "shm";

const std::string hlx::Config::SHM_DISPLAY_STR      = "Shared Memory";

const std::string hlx::Config::SHM_FIRST_TUNNEL     = "shared_memory_first";

const std::string hlx::Config::SHM_SECOND_TUNNEL    = "shared_memory_second";

const int hlx::Config::SHM_SIZE                     = 4096;

// ==================== pipe ====================

const std::string hlx::Config::PIPE_TYPE            = "pipe";

const std::string hlx::Config::PIPE_DISPLAY_STR     = "Pipe";

const std::string hlx::Config::PIPE_FIRST_TUNNEL    = "pipe_first";

const std::string hlx::Config::PIPE_SECOND_TUNNEL   = "pipe_second";

// ==================== semaphore ====================

const std::string hlx::Config::SEM_MUTEX            = "mutex";

const std::string hlx::Config::SEM_EMPTY            = "empty";

const std::string hlx::Config::SEM_FULL             = "full";
