//
//  Config.hpp
//  Parent
//
//  Created by momo on 2017/8/28.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef Config_hpp
#define Config_hpp

#include <stdio.h>

#include <string>

namespace hlx {
    class Config {
    public:
        // generic
        const static std::string FOLDER;
        const static std::string SEPERATOR;
        const static std::string UNDERLINE;
        const static std::string DOT;
        const static std::string LOCK;
        
        // shared memory
        const static std::string SHM_TYPE;
        const static std::string SHM_DISPLAY_STR;
        const static std::string SHM_FIRST_TUNNEL;
        const static std::string SHM_SECOND_TUNNEL;
        const static int SHM_SIZE;
        
        // pipe
        const static std::string PIPE_TYPE;
        const static std::string PIPE_DISPLAY_STR;
        const static std::string PIPE_FIRST_TUNNEL;
        const static std::string PIPE_SECOND_TUNNEL;
        
        // semaphore
        const static std::string SEM_MUTEX;
        const static std::string SEM_EMPTY;
        const static std::string SEM_FULL;
    };
};

#endif /* Config_hpp */
