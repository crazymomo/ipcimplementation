//
//  logger.cpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "Logger.hpp"

#include <cstring>
#include <cstdarg>

hlx::Logger* hlx::Logger::_singleton = NULL;

hlx::Logger* hlx::Logger::getInstance() {
    if (_singleton == NULL) {
        _singleton = new Logger();
    }
    return _singleton;
}

hlx::Logger::Logger() {
    
}

void hlx::Logger::init(std::string program) {
    // set program
    _program = program;
    
    // remove string berfore last /
    {
        std::size_t found = _program.find_last_of("/");
        if (found != std::string::npos) {
            _program = _program.substr(found + 1);
        }
    }
    
    // remove string after last .
    {
        std::size_t found = _program.find(".");
        if (found != std::string::npos) {
            _program = _program.substr(0, found);
        }
    }
}

void hlx::Logger::normal(bool show_program, const char* fmt, ...) {
    va_list argList;
    char msg[1024];
    
    // print fmt and arg list
    va_start(argList, fmt);
    vsprintf(msg, fmt, argList);
    va_end(argList);
    
    // print msg
    if (show_program) {
        printf("[%s] %s", _program.c_str(), msg);
    }
    else {
        printf("%s", msg);
    }
}
