//
//  Observable.cpp
//  Parent
//
//  Created by momo on 2017/8/30.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "Observable.hpp"
#include "Observer.hpp"

void hlx::Observable::addObserver(hlx::Observer *observer) {
    std::unique_lock<std::mutex> lock(_mutex);
    // do add
    _observerList.push_back(observer);
}

void hlx::Observable::removeObserver(hlx::Observer *observer) {
    std::unique_lock<std::mutex> lock(_mutex);
    // do remove
    for (auto itr = _observerList.begin(); itr != _observerList.end(); itr ++) {
        if (observer == *itr) {
            _observerList.erase(itr);
            return ;
        }
    }
}

void hlx::Observable::notifyAll(std::shared_ptr<Command> command) {
    std::unique_lock<std::mutex> lock(_mutex);
    // do notify
    for (auto itr = _observerList.begin(); itr != _observerList.end(); itr ++) {
        (*itr) -> update(command);
    }
}
