//
//  Logger.hpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef logger_hpp
#define logger_hpp

#include <stdio.h>

#include <string>

namespace hlx {
    class Logger {
    public:
        static Logger* getInstance();
        
        void init(std::string program);
        
        // message level
        void normal(bool show_program, const char* fmt, ...);
        
    protected:
        Logger();
        static Logger* _singleton;
        std::string _program;
    };
};

#endif /* logger_hpp */
