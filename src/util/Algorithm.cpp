//
//  Algorithm.cpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "Algorithm.hpp"

#include <math.h>
#include <algorithm>

double hlx::Algorithm::calcGeometricMean(std::vector<int>& list) {
    return pow(Algorithm::calcProduct(list), (double) 1.0 / (double) list.size());
}

double hlx::Algorithm::calcMean(std::vector<int> &list) {
    return Algorithm::calcSum(list) / (double) list.size();
}

double hlx::Algorithm::calcMedian(std::vector<int> &list) {
    // sort first
    std::sort(list.begin(), list.end());
    
    // odd
    if (list.size() % 2 == 1) {
        return list.at(list.size() / 2);
    }
    // even
    else {
        double sum = list.at((list.size() - 1) / 2) + list.at((list.size() - 1) / 2 + 1);
        return sum / 2;
    }
}

double hlx::Algorithm::calcSum(std::vector<int>& list) {
    double sum = 0;
    for (auto itr = list.begin(); itr != list.end(); itr ++) {
        sum += *itr;
    }
    return sum;
}

double hlx::Algorithm::calcProduct(std::vector<int>& list) {
    double product = 1;
    for (auto itr = list.begin(); itr != list.end(); itr ++) {
        product *= *itr;
    }
    return product;
}
