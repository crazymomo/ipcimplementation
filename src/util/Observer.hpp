//
//  Observer.hpp
//  Parent
//
//  Created by momo on 2017/8/30.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef Observer_hpp
#define Observer_hpp

#include <stdio.h>
#include <iostream>

#include <memory>
#include <mutex>
#include <condition_variable>

#include "Command.hpp"

namespace hlx {
    class Observer {
    public:
        virtual void update(std::shared_ptr<Command> command) = 0;
    };
};

#endif /* Observer_hpp */
