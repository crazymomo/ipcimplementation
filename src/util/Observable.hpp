//
//  Observable.hpp
//  Parent
//
//  Created by momo on 2017/8/30.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef Observable_hpp
#define Observable_hpp

#include <stdio.h>
#include <iostream>

#include <memory>
#include <mutex>
#include <condition_variable>

#include <vector>

#include "Command.hpp"

namespace hlx {
    class Observer;
    class Observable {
    public:
        void addObserver(Observer* observer);
        void removeObserver(Observer* observer);
        void notifyAll(std::shared_ptr<Command> command);
        
    protected:
        std::vector<Observer *> _observerList;
        std::mutex _mutex;
    };
}

#endif /* Observable_hpp */
