//
//  Algorithm.hpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef Algorithm_hpp
#define Algorithm_hpp

#include <stdio.h>

#include <vector>

namespace hlx {
    class Algorithm {
    public:
        static double calcGeometricMean(std::vector<int>& list);
        static double calcMean(std::vector<int>& list);
        static double calcMedian(std::vector<int>& list);
        
    protected:
        static double calcSum(std::vector<int>& list);
        static double calcProduct(std::vector<int>& list);
    };
}

#endif /* Algorithm_hpp */
