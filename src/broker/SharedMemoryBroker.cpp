//
//  SharedMemoryBroker.cpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "SharedMemoryBroker.hpp"

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sstream>
#include <cstring>

#include <vector>

#include "Config.hpp"

#include <assert.h>

hlx::SharedMemoryBroker::SharedMemoryBroker()
: hlx::DataBroker(Config::SHM_DISPLAY_STR) {
    // init sem list
    std::unordered_map<std::string, int> _semValueMap;
    _semValueMap[Config::SEM_MUTEX] = 1;
    _semValueMap[Config::SEM_EMPTY] = 1;
    _semValueMap[Config::SEM_FULL] = 0;
    
    // get lock path
    std::string lock_file_path = getLockPath(hlx::Config::SHM_FIRST_TUNNEL);
    
    // check lock
    int lock_file_fd = open(lock_file_path.c_str(), O_CREAT | O_RDWR, 0644);
    if (flock(lock_file_fd, LOCK_EX | LOCK_NB) == 0) {
        // set tunnel list
        _tunnelList.push_back(Config::SHM_FIRST_TUNNEL);
        _tunnelList.push_back(Config::SHM_SECOND_TUNNEL);

        // iterator tunnel list
        for (auto itr = _tunnelList.begin(); itr != _tunnelList.end(); itr ++) {
            // create sem list
            for (auto sem_itr = _semValueMap.begin(); sem_itr != _semValueMap.end(); sem_itr ++) {
                // create sem
                sem_unlink(getSemaphorePath(*itr, sem_itr -> first).c_str());
                sem_t* sem = sem_open(getSemaphorePath(*itr, sem_itr -> first).c_str(), O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, sem_itr -> second);
                assert(sem != SEM_FAILED);
                // add sem to sem map
                _semMap[getSemaphorePath(*itr, sem_itr -> first)] = sem;
            }
            
            // create shm list
            std::string tunnel_path = getTunnelPath(*itr);
            unlink(tunnel_path.c_str());
            int fd = open(tunnel_path.c_str(), O_CREAT | O_RDWR | O_TRUNC, 0644);
            lseek(fd, 0, SEEK_END);
            ::write(fd, "", 1);
            _shmMap[*itr] = mmap(NULL, Config::SHM_SIZE, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0);
            ::close(fd);
        }
    }
    else {
        // set tunnel list
        _tunnelList.push_back(Config::SHM_SECOND_TUNNEL);
        _tunnelList.push_back(Config::SHM_FIRST_TUNNEL);
        
        // iterator tunnel list
        for (auto itr = _tunnelList.begin(); itr != _tunnelList.end(); itr ++) {
            // create sem list
            for (auto sem_itr = _semValueMap.begin(); sem_itr != _semValueMap.end(); sem_itr ++) {
                // create sem
                sem_t* sem = sem_open(getSemaphorePath(*itr, sem_itr -> first).c_str(), O_RDWR);
                assert(sem != SEM_FAILED);
                // add sem to sem map
                _semMap[getSemaphorePath(*itr, sem_itr -> first)] = sem;
            }
            
            // create shm list
            std::string tunnel_path = getTunnelPath(*itr);
            int fd = open(tunnel_path.c_str(), O_RDWR);
            _shmMap[*itr] = mmap(NULL, Config::SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        }
    }
}

// ========================= generic method =========================

std::string hlx::SharedMemoryBroker::getSemaphorePath(std::string tunnel, std::string extension) {
    std::stringstream ss;
    ss << tunnel << Config::UNDERLINE << extension;
    return ss.str();
}

sem_t* hlx::SharedMemoryBroker::getSemaphore(std::string tunnel, std::string extension) {
    assert(_semMap.find(getSemaphorePath(tunnel, extension)) != _semMap.end());
    return _semMap[getSemaphorePath(tunnel, extension)];
}

// ========================= read / write method =========================

void hlx::SharedMemoryBroker::read(char *byte, int len) {
    // wait semaphore full
    sem_wait(getSemaphore(_tunnelList.at(1), Config::SEM_FULL));
    
    // copy from memory
    sem_wait(getSemaphore(_tunnelList.at(1), Config::SEM_MUTEX));
    memcpy(byte, _shmMap[_tunnelList.at(1)], len);
    sem_post(getSemaphore(_tunnelList.at(1), Config::SEM_MUTEX));
    
    // release semaphore empty
    sem_post(getSemaphore(_tunnelList.at(1), Config::SEM_EMPTY));
}

void hlx::SharedMemoryBroker::write(char *byte, int len) {
    // wait semaphore empty
    sem_wait(getSemaphore(_tunnelList.at(0), Config::SEM_EMPTY));
    
    // copy to memory
    sem_wait(getSemaphore(_tunnelList.at(0), Config::SEM_MUTEX));
    memcpy(_shmMap[_tunnelList.at(0)], byte, len);
    sem_post(getSemaphore(_tunnelList.at(0), Config::SEM_MUTEX));
    
    // release semaphore full
    sem_post(getSemaphore(_tunnelList.at(0), Config::SEM_FULL));
}

void hlx::SharedMemoryBroker::close() {
    _alive = false;
    // close sem
    for (auto itr = _semMap.begin(); itr != _semMap.end(); itr ++) {
        sem_close(itr -> second);
    }
    // close share memory
    for (auto itr = _shmMap.begin(); itr != _shmMap.end(); itr ++) {
        munmap(itr -> second, Config::SHM_SIZE);
    }
    _semMap.clear();
}
