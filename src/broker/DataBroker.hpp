//
//  DataBroker.hpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef DataBroker_hpp
#define DataBroker_hpp

#include <stdio.h>

#include <string>
#include <vector>

namespace hlx {
    class DataBroker {
    public:
        DataBroker(std::string displayStr);
        
        virtual inline std::string getDisplayStr() { return _displayStr; };
        
        virtual inline bool isAlive() { return _alive; };
        
        virtual void read(char* byte, int len) = 0;
        virtual void write(char* byte, int len) = 0;
        virtual void close() = 0;
        
    protected:
        // generic method
        std::string getLockPath(std::string tunnel);
        std::string getTunnelPath(std::string tunnel);
       
        // type name
        std::string _displayStr;
        bool _alive;
        
        // tunnel list
        std::vector<std::string> _tunnelList;
    };
};

#endif /* DataBroker_hpp */
