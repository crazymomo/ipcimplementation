//
//  PipeBroker.hpp
//  Parent
//
//  Created by momo on 2017/8/28.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef PipeBroker_hpp
#define PipeBroker_hpp

#include <stdio.h>

#include <unordered_map>

#include "DataBroker.hpp"

namespace hlx {
    class PipeBroker : public DataBroker {
    public:
        PipeBroker();
        
        // read / write method
        virtual void read(char* byte, int len);
        virtual void write(char* byte, int len);
        virtual void close();
        
    protected:
        std::unordered_map<std::string, int> _pipeMap;
    };
};

#endif /* PipeBroker_hpp */
