//
//  PipeBroker.cpp
//  Parent
//
//  Created by momo on 2017/8/28.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "PipeBroker.hpp"

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/file.h>
#include <fcntl.h>

#include "Config.hpp"

hlx::PipeBroker::PipeBroker()
: hlx::DataBroker(Config::PIPE_DISPLAY_STR) {
    // get lock path
    std::string lock_file_path = getLockPath(Config::PIPE_FIRST_TUNNEL);
    
    // check lock file
    int lock_file_fd = open(lock_file_path.c_str(), O_CREAT | O_RDWR, 0644);
    if (flock(lock_file_fd, LOCK_EX | LOCK_NB) == false) {
        // set tunnel list
        _tunnelList.push_back(Config::PIPE_FIRST_TUNNEL);
        _tunnelList.push_back(Config::PIPE_SECOND_TUNNEL);
        
        // iterator tunnel list
        for (auto itr = _tunnelList.begin(); itr != _tunnelList.end(); itr ++) {
            // create pipe
            std::string tunnel_path = getTunnelPath(*itr);
            mkfifo(tunnel_path.c_str(), 0644);
            _pipeMap[*itr] = open(tunnel_path.c_str(), O_RDWR);
        }
    }
    else {
        // set tunnel list
        _tunnelList.push_back(Config::PIPE_SECOND_TUNNEL);
        _tunnelList.push_back(Config::PIPE_FIRST_TUNNEL);
        
        // iterator tunnel list
        for (auto itr = _tunnelList.begin(); itr != _tunnelList.end(); itr ++) {
            // add pipe list
            std::string tunnel_path = getTunnelPath(*itr);
            _pipeMap[*itr] = open(tunnel_path.c_str(), O_RDWR);
        }
    }
}

void hlx::PipeBroker::read(char *byte, int len) {
    ::read(_pipeMap[_tunnelList.at(1)], byte, len);
}

void hlx::PipeBroker::write(char *byte, int len) {
    ::write(_pipeMap[_tunnelList.at(0)], byte, len);
}

void hlx::PipeBroker::close() {
    _alive = false;
    ::close(_pipeMap[_tunnelList.at(0)]);
    ::close(_pipeMap[_tunnelList.at(1)]);
}
