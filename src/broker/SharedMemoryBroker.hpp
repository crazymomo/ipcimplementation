//
//  SharedMemoryBroker.hpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef SharedMemoryBroker_hpp
#define SharedMemoryBroker_hpp

#include <stdio.h>
#include <string>
#include <semaphore.h>
#include <vector>
#include <unordered_map>

#include "DataBroker.hpp"

namespace hlx {
    class SharedMemoryBroker : public DataBroker {
    public:
        SharedMemoryBroker();
        
        // read / write method
        void read(char* byte, int len);
        void write(char* byte, int len);
        void close();
        
    protected:
        // generic method
        std::string getSemaphorePath(std::string tunnel, std::string extension);
        sem_t* getSemaphore(std::string tunnel, std::string extension);
        
        // semaphore map
        std::unordered_map<std::string, sem_t *> _semMap;
        
        // shared memory map
        std::unordered_map<std::string, void *> _shmMap;
    };
};

#endif /* SharedMemoryBroker_hpp */
