//
//  DataBroker.cpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "DataBroker.hpp"

#include <sstream>

#include "Config.hpp"

hlx::DataBroker::DataBroker(std::string displayStr) {
    _displayStr = displayStr;
    _alive = true;
}

// ========================= generic method =========================
std::string hlx::DataBroker::getLockPath(std::string tunnel) {
    std::stringstream ss;
    ss << Config::FOLDER << Config::SEPERATOR << tunnel << Config::UNDERLINE << Config::LOCK;
    return ss.str();
}

std::string hlx::DataBroker::getTunnelPath(std::string tunnel) {
    std::stringstream ss;
    ss << Config::FOLDER << Config::SEPERATOR << tunnel;
    return ss.str();
}
