//
//  Command.hpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef Command_hpp
#define Command_hpp

#include <stdio.h>

namespace hlx {
    class Command {
    public:
        enum TYPE {
            NONE = 0,
            EXIT,
            WILL_ADD, DID_ADD, ADD,
            CALC_MEDIAN, CALC_GEOMETRIC_MEAN,
            SORT, CLEAR,
            END_INIT, END_CALC,
        };
        
        Command(TYPE type = NONE, int value = 0);
        
        TYPE _type;
        int _value;
    };
};

#endif /* Command_hpp */
