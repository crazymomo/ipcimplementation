//
//  CommandHandler.cpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#include "CommandHandler.hpp"

#include "PipeBroker.hpp"
#include "SharedMemoryBroker.hpp"

#include "Config.hpp"
#include "Algorithm.hpp"
#include "Logger.hpp"

#include <assert.h>

hlx::CommandHandler::CommandHandler(std::string type) {
    // set broker
    if (type == Config::PIPE_TYPE) {
        _broker = std::shared_ptr<DataBroker>(new PipeBroker());
    }
    else if (type == Config::SHM_TYPE) {
        _broker = std::shared_ptr<DataBroker>(new SharedMemoryBroker());
    }
    else {
        assert(false);
    }
    
    // launch reader and writer thread
    pthread_create(&_reader, NULL, &CommandHandler::readThread, this);
    pthread_create(&_writer, NULL, &CommandHandler::writeThread, this);
}

// ========================= read / write threads =========================

void* hlx::CommandHandler::readThread(void *self) {
    ((CommandHandler *) self) -> readThread();
    return NULL;
}

void hlx::CommandHandler::readThread() {
    while (_broker -> isAlive()) {
        // do read
        std::shared_ptr<hlx::Command> command = std::shared_ptr<hlx::Command>(new hlx::Command);
        _broker -> read((char *) command.get(), sizeof(command.get()));
        
        // check type
        if (command -> _type == Command::NONE)
            continue ;
        
        // do notify all
        notifyAll(command);
    }
}

void* hlx::CommandHandler::writeThread(void *self) {
    ((CommandHandler *) self) -> writeThread();
    return NULL;
}

void hlx::CommandHandler::writeThread() {
    while (_broker -> isAlive()) {
        // get lock
        std::unique_lock<std::mutex> lock(_mutex);
        
        // wait command
        while (_queue.empty()) {
            std::chrono::seconds sec(1);
            _cv.wait_for(lock, sec);
        }
        
        // get command
        std::shared_ptr<Command> command = _queue.front();
        _queue.pop();
        
        // write command
        _broker -> write((char *) command.get(), sizeof(command.get()));
    }
}

// ========================= read / write functions =========================

void hlx::CommandHandler::doRead() {
    pthread_join(_reader, NULL);
}

void hlx::CommandHandler::doWrite(std::shared_ptr<hlx::Command> command) {
    // get lock
    std::unique_lock<std::mutex> lock(_mutex);
    
    // add to queue
    _queue.push(command);
}

void hlx::CommandHandler::doClose() {
    _broker -> close();
}
