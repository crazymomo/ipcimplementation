//
//  CommandHandler.hpp
//  Parent
//
//  Created by momo on 2017/8/27.
//  Copyright © 2017年 momo. All rights reserved.
//

#ifndef CommandHandler_hpp
#define CommandHandler_hpp

#include <stdio.h>
#include <iostream>
#include <queue>

#include <memory>
#include <mutex>
#include <condition_variable>

#include "Observable.hpp"
#include "DataBroker.hpp"
#include "Command.hpp"

namespace hlx {
    class CommandHandler : public Observable {
    public:
        CommandHandler(std::string type);
        
        // get type
        virtual inline std::string getDisplayStr() { return _broker -> getDisplayStr(); };
        
        // write threads
        static void* readThread(void* self);
        void readThread();
        static void* writeThread(void* self);
        void writeThread();
        
        // read / write / close functions
        virtual void doRead();
        virtual void doWrite(std::shared_ptr<Command> command);
        virtual void doClose();
        
    protected:
        // current data broker such as pipe or shared memory
        std::shared_ptr<DataBroker> _broker;
        
        // reader and writer
        pthread_t _reader;
        pthread_t _writer;
        
        // command queue
        std::queue<std::shared_ptr<Command>> _queue;
        std::mutex _mutex;
        std::condition_variable _cv;
    };
}

#endif /* CommandHandler_hpp */
