//
//  childA.cpp
//  ChildWithPipe
//
//  Created by momo on 2017/8/28.
//  Copyright © 2017年 momo. All rights reserved.
//

#include <stdio.h>

#include <assert.h>

#include "CalculationClient.hpp"
#include "Config.hpp"
#include "Logger.hpp"

int main(int argc, char** argv) {
    // init logger
    hlx::Logger::getInstance() -> init(argv[0]);
    
    // show logs
    hlx::Logger::getInstance() -> normal(true, "Child process started\n");
    
    // init calculatin client
    hlx::CalculationClient client(hlx::Config::PIPE_TYPE);
    client.start();
    
    // show logs
    hlx::Logger::getInstance() -> normal(true, "Child process exits\n");
    
    return 0;
}
