### Introduction

  - All the source files are in **src** folder.
  - you can use **make_and_run.sh** to launch mainParent, childA and childB directly.

### Todos

  - It's better to send **multiple** commands at one time in order to enhance performance.
  - For interview purpose, this solution doesn't include third party libraries such as **Boost** and **Libevent**.  If there will br the requirement like supporting **cross platform**, should add it. 
  - The deveploment environemnt is **MacBook Pro (Retina, 15-inch, Mid 2015)**.
